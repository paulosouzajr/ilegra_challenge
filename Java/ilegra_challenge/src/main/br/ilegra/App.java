package main.br.ilegra;

import java.io.File;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class App {
    public static void main(String args[]) throws InterruptedException {
        BlockingQueue<File> queue =
                new LinkedBlockingQueue<>();

        String mainPath = System.getProperty("user.home") + "/data/in";
        File dir = new File(mainPath);

        System.out.println("Starting Integrator");

        Thread p1 = new Thread(new Consumer(queue));

        p1.start();

        while (true){
            File[] files = dir.listFiles((d, name) -> name.endsWith(".dat"));

            for (File f:files) {
                if (f.isFile() && !queue.contains(f)) {
                    queue.put(f);
                }
            }
            Thread.sleep(1000);
        }

    }

}
