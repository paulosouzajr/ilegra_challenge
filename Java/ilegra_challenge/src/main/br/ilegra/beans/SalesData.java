package main.br.ilegra.beans;

import java.math.BigDecimal;
import java.util.ArrayList;

public class SalesData extends Record{
    private String salesID;
    private ArrayList<Sale> sales;

    public SalesData(String salesID, String sales, String name) {
        super.setName(name);
        this.salesID = salesID;
        this.sales = parserSales(sales);
    }

    private ArrayList<Sale> parserSales(String sales){
        ArrayList<Sale> aux = new ArrayList<>();
        for (String sale: sales.split(",")){
            String value[] = sale.split("-");
            aux.add(new Sale(value[0].replace("[", ""),
                    Integer.parseInt(value[1]), new BigDecimal(value[2].replace("]", ""))));
        }
        return aux;

    }

    public String getSalesID() {
        return salesID;
    }

    public void setSalesID(String salesID) {
        this.salesID = salesID;
    }

    public ArrayList<Sale> getSales() {
        return sales;
    }

    public void setSales(ArrayList<Sale> sales) {
        this.sales = sales;
    }

    public BigDecimal getSalesSum(){
        BigDecimal accumulator = BigDecimal.ZERO;
        for (Sale sale: sales){
            accumulator = accumulator.add(sale.getItemPrice().multiply(new BigDecimal(sale.getItemQuantity())));
        }
        return accumulator;
    }
}
