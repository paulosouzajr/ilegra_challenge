package main.br.ilegra.beans;

import java.math.BigDecimal;

public class SaleInfo {
    public String id = "";
    public BigDecimal value = BigDecimal.ZERO;
    public String salesman = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public String getSalesman() {
        return salesman;
    }

    public void setSalesman(String salesman) {
        this.salesman = salesman;
    }

}
