package main.br.ilegra.beans;

import java.math.BigDecimal;

public class Salesman extends Record{
    private String cpf;
    private BigDecimal salary;
    public BigDecimal revenue;

    public Salesman(String name, String cpf, BigDecimal salary) {
        super.setName(name.replace("\n", ""));
        this.cpf = cpf;
        this.salary = salary;
        this.revenue = BigDecimal.ZERO;
    }


    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public BigDecimal getRevenue() {
        return revenue;
    }

    public void setRevenue(BigDecimal revenue) {
        this.revenue = revenue;
    }

    public void increaseRevenue(BigDecimal value){
        this.revenue = this.revenue.add(value);
    }
}
