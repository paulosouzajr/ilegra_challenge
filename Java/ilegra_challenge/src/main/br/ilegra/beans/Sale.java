package main.br.ilegra.beans;

import java.math.BigDecimal;

public class Sale {
    private String itemID;
    private int itemQuantity;
    private BigDecimal itemPrice;

    public Sale(String itemID, int itemQuantity, BigDecimal itemPrice) {
        this.itemID = itemID;
        this.itemQuantity = itemQuantity;
        this.itemPrice = itemPrice;
    }

    public String getItemID() {
        return itemID;
    }

    public void setItemID(String itemID) {
        this.itemID = itemID;
    }

    public int getItemQuantity() {
        return itemQuantity;
    }

    public void setItemQuantity(int itemQuantity) {
        this.itemQuantity = itemQuantity;
    }

    public BigDecimal getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(BigDecimal itemPrice) {
        this.itemPrice = itemPrice;
    }
}
