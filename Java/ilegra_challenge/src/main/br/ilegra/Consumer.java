package main.br.ilegra;

import main.br.ilegra.beans.SaleInfo;
import main.br.ilegra.beans.SalesData;
import main.br.ilegra.beans.Salesman;

import java.io.*;
import java.math.BigDecimal;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable {

    private final BlockingQueue<File> queue;

    public Consumer(BlockingQueue<File> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        File file;
        while (!Thread.currentThread().isInterrupted()) {
            try {
                if (!queue.isEmpty()) {
                    file = queue.take();
                    if (processData(file)) System.out.println("File processed with success: " + file.getName());
                    else System.out.println("something went wrong");
                } else {
                    Thread.sleep(5000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private String worst_seller(ArrayList<Salesman> salesmen) {
        Salesman aux = salesmen.get(0);

        for (Salesman salesman : salesmen) {
            if (salesman.getRevenue().compareTo(aux.getRevenue()) <= 0) {
                aux.setName(salesman.getName());
                aux.setRevenue(salesman.getRevenue());
            }
        }

        return aux.getName();
    }

    private Boolean processData(File file) {
        File temp = renameFile(file);
        int customers = 0;

        ArrayList<Salesman> salesmen = new ArrayList<>();
        SalesData salesData;

        SaleInfo expensiveSaleInfo = new SaleInfo();

        if (!temp.exists()) return Boolean.FALSE;

        try {
            BufferedReader br = new BufferedReader(new FileReader(temp.getAbsolutePath()));
            String[] data;
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                data = line.split("ç");

                switch (data[0]) {
                    case "001":
                        salesmen.add(new Salesman(data[2], data[1], new BigDecimal(data[3])));
                        break;

                    case "002":
                        customers += 1;
                        break;

                    case "003":
                        salesData = new SalesData(data[1], data[2], data[3]);

                        BigDecimal sum = salesData.getSalesSum();

                        if (expensiveSaleInfo.value.compareTo(sum) < 0) {
                            expensiveSaleInfo.id = salesData.getSalesID();
                            expensiveSaleInfo.salesman = salesData.getName();
                            expensiveSaleInfo.value = sum;
                        }

                        for (int i = 0; i < salesmen.size(); i++) {
                            if (salesmen.get(i).getName().equals(salesData.getName())) {
                                salesmen.get(i).increaseRevenue(salesData.getSalesSum());
                                break;
                            }
                        }

                        break;

                    default: //invalid line
                        break;
                }
            }

            br.close();

            temp.delete();

            String outputPath = System.getProperty("user.home") + "/data/out/";

            Path outputFile = Paths.get(temp.getName());

            BufferedWriter writer = new BufferedWriter(new FileWriter(outputPath + outputFile.getFileName().toString().replace(".temp", ".done.dat")));

            writer.write("Amount of clients in the input file: " + customers + "\n");
            writer.write("Amount of salesman in the input file: " + salesmen.size() + "\n");
            writer.write("ID of the most expensive sale " + expensiveSaleInfo.getId() + "\n");
            writer.write("Worst salesman ever " + worst_seller(salesmen) + "\n");

            writer.close();

        } catch (FileNotFoundException e) {
            System.out.println(e);
            return false;
        } catch (IOException e) {
            return false;
        }

        return Boolean.TRUE;
    }

    private File renameFile(File file) {
        File newFile = new File(file.getAbsolutePath().replace(".dat", ".temp"));
        if (file.isFile() && file.exists()) {
            if (file.renameTo(newFile)) {
                return newFile;
            }
        }

        return new File("");
    }

}
