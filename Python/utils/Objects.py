class SalesData(object):
    """SalesData

    :param id: a string, SalesData's Sale Id.
    :param sales: a list with the sales.
    :param seller: a float, SalesMan's name.
    """

    def __init__(self, id='', sales=list(), seller=''):
        self.id = id
        self.sales = self.removebrackets(sales)
        self.seller = seller.replace('\n', '')

    @staticmethod
    def removebrackets(sales):
        return [[dt.replace('[', '').replace(']', '') for dt in data] for data in sales]

    def salesum(self):
        sum = 0
        for i in self.sales:
            i[2] = i[2]
            sum += int(i[1]) * float(i[2])
        return sum


class Customer(object):
    """Customer Data

    :param cnpj: a string, Customer's cnpj.
    :param name: a string, Customer's name.
    :param area: a string, Customer's BusinessArea.
    """

    def __init__(self, cnpj='', name='', area=''):
        self.cnpj = cnpj
        self.name = name
        self.area = area


class Salesman(object):
    """Salesman Data

    :param cpf: a int, Salesman's cpf.
    :param name: a string, Salesman's name.
    :param salary: a float, Salesman's salary.
    """

    def __init__(self, cpf='', name='', salary=0):
        self.cpf = cpf
        self.name = name.replace('\n', '')
        self.salary = salary
        self.total = 0

    def addtototal(self, value):
        self.total += value