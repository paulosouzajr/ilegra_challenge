import os
import queue
import time
from datetime import datetime
from os.path import isfile, join
from pathlib import Path
from threading import Thread

from utils import Objects

def worst_seller(ls):
    seller = {
        'name': ls[0].name,
        'value': ls[0].total,
    }

    for salesman in ls:
        if salesman.total < seller['value']:
            seller['name'] = salesman.name
            seller['value'] = salesman.total

    return seller["name"]


def process_data(file):
    customers = 0
    salesmen = 0

    expensive_sale = {
        'id': '',
        'value': 0,
        'salesman': ''
    }

    temp_file = file.replace('.dat', '.temp')

    #produces the temporary file that will be processed
    #this is necessary to avoid duplicated consumption of files
    if os.path.isfile(file):
        os.rename(file, temp_file)
        print('creating temp ', temp_file)
    else:
        return

    #if any exception restores file and finish execution
    try:
        #reads the temp file
        #the read operation is made using a cursor, that reads each line of the file, without loading everything in the memory (which is sometimes insanity)
        #data is handled in the moment it is loaded optimizate memory usage
        with open(temp_file, 'r') as f:
            salesmen = []
            for line in f:
                data = line.split('ç')
                if '001' in data[0]:
                    salesmen.append(Objects.Salesman(data[1], data[2], data[3]))
                elif '002' in data[0]:
                    customers += 1
                elif '003' in data[0]:
                    salesdata = Objects.SalesData(data[1],
                                                  [f.split('-') for f in
                                                   data[2].split(',')], data[3])
                    sum = salesdata.salesum()

                    if expensive_sale['value'] < sum:
                        expensive_sale['id'] = salesdata.id
                        expensive_sale['value'] = sum
                        expensive_sale['salesman'] = salesdata.seller

                    #if too much salesman this will not be appropriated, since this point of the algorithm has a high complexity
                    #perhaps sorting and using binary search may slightly improve execution 
                    #or save all data, group by name and sum results; however this is bad for memory
                    for index, i in enumerate(salesmen):
                        if i.name == salesdata.seller:
                            salesmen[index].addtototal(sum)
                            break
    except Exception:
        os.rename(temp_file, file)
        print("Problem in file: ", file)
        return

    f.close()
    os.remove(temp_file)

    output_path = str(Path.home()) + '/data/out/'

    output_file = output_path + os.path.basename(file)

    #write output results
    with open(output_file.replace('.dat', '.done.dat'), 'w') as f:
        f.write('Amount of clients in the input file: {}\n'
                'Amount of salesman in the input file: {} \n'
                'ID of the most expensive sale {} \n'
                'Worst salesman ever {}'.format(customers, len(salesmen), expensive_sale['id'], worst_seller(salesmen)))
    f.close()

#function that consumes informations from the queue
#when there is a file on the queue, the file is removed from it and processed
def watcher(q):
    while True:
        if not q.empty():
            start = datetime.now()
            file = q.get()
            process_data(file)
            print("Work done in file: {} with {}".format(file, datetime.now() - start))
            q.task_done()
        else:
            time.sleep(2)


if __name__ == '__main__':
    path = str(Path.home()) + '/data/in/'

    q = queue.Queue()

    #the number of threads will not impact the execution time, since the application is IO bound. 
    #increasing the number of threads may increase concurrence to disk usage and delay the process.
    #perhaps reading files by batch size would present better results, but it depends on the number of operations running alongside the processes
    #it is only usefull for the queue priority and execution 
    for w in range(2):
        t = Thread(target=watcher, name='worker-%s' % w, args=(q,))
        t.daemon = True
        t.start()

    while True:
        onlyfiles = [f for f in os.listdir(path) if isfile(join(path, f)) and '.dat' in join(path, f)]
        for f in onlyfiles:
            if path + f not in q.queue:
                q.put(path + f)

        print('Queue size: ', q.qsize())
        onlyfiles = ''
        time.sleep(10)
